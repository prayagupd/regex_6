import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * 2)  Write a regular expression for a license plate number whose format is [two letters] 
 * followed by [four digits] followed by [three letters].
 * 
 * 3) Replace every occurrence of  8  with  ‘eight’.
 * @author prayagupd
 *
 */
public class RegexApp {
	public static final String LISCENCE_PATTERN = "[a-zA-Z]{2}[0-9]{4}[a-zA-Z]{3}";
	
	public static void main(String[] args) {
		
		String licenseNumber = "AA5088PRU";
		
		if (isValid(licenseNumber)) {
			System.out.println("Valid Liscence Number.");
			System.out.println(replaceWithEight(licenseNumber));
		} else {
			System.out.println("Invalid Liscence Number.");
		}
		
		System.out.println(System.lineSeparator());
	}
	
	public static boolean isValid(String licenseNumber) {
		return Pattern.compile(LISCENCE_PATTERN).matcher(licenseNumber).matches();
	}

	public static String replaceWithEight(String string) {
		return string.replaceAll("8", "eight");
	}

}
